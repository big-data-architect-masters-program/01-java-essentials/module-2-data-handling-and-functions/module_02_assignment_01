import java.util.Arrays;
import java.util.Scanner;

public class Statement3 {
    public static void main(String[] args) {
        int size = 5;
        String[] id = new String[size];
        String[] name = new String[size];
        String[] salary = new String[size];
        Scanner sc = new Scanner(System.in);
        System.out.println("Add 5 Employees:");
        for (int i = 0; i < size; i++) {
            System.out.print("ID-" + (i + 1) + ": ");
            id[i] = sc.next();
            System.out.print("NAME-" + (i + 1) + ": ");
            name[i] = sc.next();
            System.out.print("SALARY-" + (i + 1) + ": ");
            salary[i] = sc.next();
        }
        display(id, name, salary);
        display(id, name);
        System.out.print("Search by Employee Name: ");
        String search = sc.next();
        display(search, id, name, salary); // for statement 3
    }

    static void display(String[] id, String[] name, String[] salary) {

        System.out.println("\n\n----------------------");
        System.out.println("EMPLOYEE LIST 1:");
        System.out.println("----------------------");
        String leftAlignFormat = "| %-3s | %-15s | %-8s |%n";

        System.out.format("+-----+-----------------+----------+%n");
        System.out.format("| ID  | NAME            | SALARY   |%n");
        System.out.format("+-----+-----------------+----------+%n");

        for (int i = 0; i < id.length; i++) {
            System.out.format(leftAlignFormat, id[i], name[i], salary[i]);
        }
        System.out.format("+-----+-----------------+----------+%n");
    }

    static void display(String[] id, String[] name) {

        System.out.println("\n\n----------------------");
        System.out.println("EMPLOYEE LIST 2:");
        System.out.println("----------------------");
        String leftAlignFormat = "| %-3s | %-15s |%n";

        System.out.format("+-----+-----------------+%n");
        System.out.format("| ID  | NAME            |%n");
        System.out.format("+-----+-----------------+%n");

        for (int i = 0; i < id.length; i++) {
            System.out.format(leftAlignFormat, id[i], name[i]);
        }
        System.out.format("+-----+-----------------+%n");
    }

    static void display(String searchName, String[] id, String[] name, String[] salary) {

        // find index of searchName
        int i = Arrays.asList(name).indexOf(searchName);

        System.out.println("\n\n----------------------");
        System.out.println("FOUND EMPLOYEE:");
        System.out.println("----------------------");
        String leftAlignFormat = "| %-3s | %-15s | %-8s |%n";

        System.out.format("+-----+-----------------+----------+%n");
        System.out.format("| ID  | NAME            | SALARY   |%n");
        System.out.format("+-----+-----------------+----------+%n");

        System.out.format(leftAlignFormat, id[i], name[i], salary[i]);

        System.out.format("+-----+-----------------+----------+%n");
    }
}
