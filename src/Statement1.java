import java.util.Scanner;

public class Statement1 {
    public static void main(String[] args) {
        int size = 5;
        String[] id = new String[size];
        String[] name = new String[size];
        String[] salary = new String[size];
        Scanner sc = new Scanner(System.in);
        System.out.println("Add 5 Employees:");
        for (int i = 0; i < size; i++) {
            System.out.print("ID-" + (i+1) + ": ");
            id[i] = sc.next();
            System.out.print("NAME-" + (i+1) + ": ");
            name[i] = sc.next();
            System.out.print("SALARY-" + (i+1) + ": ");
            salary[i] = sc.next();
        }
        display(id, name, salary);
    }

    static void display(String[] id, String[] name, String[] salary) {

        System.out.println("\n\n----------------------");
        System.out.println("EMPLOYEE PROGRAM DEMO:");
        System.out.println("----------------------");
        String leftAlignFormat = "| %-3s | %-15s | %-8s |%n";

        System.out.format("+-----+-----------------+----------+%n");
        System.out.format("| ID  | NAME            | SALARY   |%n");
        System.out.format("+-----+-----------------+----------+%n");

        for (int i = 0; i < id.length; i++) {
            System.out.format(leftAlignFormat, id[i], name[i],salary[i]);
        }
        System.out.format("+-----+-----------------+----------+%n");
    }
}
